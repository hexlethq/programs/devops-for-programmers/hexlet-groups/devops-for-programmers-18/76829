# Zero-downtime deployment

Реализуйте zero-downtime deployment  с помощью Kubernetes по стратегии Rolling Update (эта стратегия используется по умолчанию). Убедитесь, что во время деплоя приложение будет доступно без прерывания.

## Ссылки

* [Six Strategies for Application Deployment](https://thenewstack.io/deployment-strategies/) - описание стратегий деплоя с визуализацией и сравнением
* [Чек-лист хороших инженерных практик в компаниях](https://guides.hexlet.io/check-list-of-engineering-practices/)
* [Среды разработки](https://ru.hexlet.io/blog/posts/environment)

## Задачи

* Соберите и запуште на Dockerhub свой образ на базе [devops-example-app](https://github.com/hexlet-components/devops-example-app), укажите версию (тег), например - `yourname/devops-example-app:v1`
* Создайте в Digital Ocean отдельный сервер (дроплет) для мониторинга
* Установите на него агент Datadog
* Добавьте хелфчек (healthcheck), который будет запрашивать внешний адрес приложения, которое находится в кластере Kubernetes. Таким образом проверяя его текущее состояние
* Используйте созданный ранее образ в файлах конфигурации деплоя. Выполните и промониторьте деплой приложения
* Создайте новый релиз с новым тегом (например - `yourname/devops-example-app:v2`) и задеплойте его. Убедитесь, что в этот приложение было доступно и мониторинг не сработал
* Откатите релиз на предыдущую версию (измените тег образа)

Таким образом в любой момент времени приложение будет доступно с новой или старой версией.

## Решение
[kizilov.club](http://kizilov.club)

### Requirements
* make,
  docker,
  ansible,
  ansible-galaxy,
  kubectl (kubernetes)
* accounts:
  DO - one droplet, one kuber instance - 2 replica + loadBalancer
  dockerhub
  DataDog
  
### Setup
* Build img for deploy:
    * setup img, img version for dockerhub (will build) - `Makefile`
    * SERVER_MESSAGE in `.env` for current docker image
    * `make add-img`
    * repeat for some versions
* Setup kuber
    * config in `Makefile`
    * image in `app-deployment.yaml` from dockerhub (DOCKER_IMG from `Makefile` same)
    * check forwarding ports in `app-service.yaml`
* add `vault-password` with password
* setup DataDog:
    * add api key: `datadog/group_vars/webservers/secrets.yml`
    * change settings: `datadog/group_vars/webservers/dataDog.yml`
    * setup droplet ip: `datadog/inventory.ini`
    * `make dd-setup` add datadog role
    * `make dd-secrets` encrypt datadog api key (`make dd-opensec` to open api key)
* Deploy:
    * app to kuber: `make apply`
    * get external ip for kube: `make getsrv` - setup url for check rule datadog `datadog/group_vars/webservers/dataDog.yml`
    * datadog to droplet: `make dd-deploy`
* setup monitor at datadog account type network

### Redeploy app
* change image version at: `Makefile`, `app-deployment.yaml`
* `make apply`
