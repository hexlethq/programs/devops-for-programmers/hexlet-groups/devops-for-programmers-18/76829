dd-setup:
	ansible-galaxy install -r datadog/requirements.yml

dd-deploy:
	ansible-playbook --ask-vault-pass datadog/playbook.yml -i datadog/inventory.ini

dd-secrets:
	ansible-vault encrypt --vault-password-file vault-password datadog/group_vars/webservers/secrets.yml
	#ansible-vault encrypt_string --vault-password-file vault-password 'your secret message' --name

dd-opensec:
	ansible-vault decrypt --vault-password-file vault-password datadog/group_vars/webservers/secrets.yml
