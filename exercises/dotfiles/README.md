# Setup [o-my-zsh](https://ohmyz.sh/) and dotfiles

## Requirements
* OS - Ubuntu
* installed - [make](https://guides.hexlet.io/makefile-as-task-runner/), [docker](https://docs.docker.com/engine/install/)

## Setup

```sh
make setup
make dotfiles
```
