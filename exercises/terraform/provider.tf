terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "dietmar" {
  name = "dietmar"
}

resource "digitalocean_droplet" "web01" {
  image = "docker-20-04"
  name = "web-terraform-homework-01"
  region = "ams3"
  size = "s-1vcpu-1gb"
  tags = ["terra"]
  ssh_keys = [data.digitalocean_ssh_key.dietmar.id]
}

resource "digitalocean_droplet" "web02" {
  image = "docker-20-04"
  name = "web-terraform-homework-02"
  region = "ams3"
  size = "s-1vcpu-1gb"
  tags = ["terra"]
  ssh_keys = [data.digitalocean_ssh_key.dietmar.id]
}

resource "digitalocean_loadbalancer" "lb-terra" {
  name   = "lb-terra"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_tag = "terra"
}

resource "digitalocean_domain" "default" {
  name       = "terra.kizilov.club"
  ip_address = digitalocean_loadbalancer.lb-terra.ip
}

output "output_ips" {
  value = {
    "web-terraform-homework-01": digitalocean_droplet.web01.ipv4_address,
    "web-terraform-homework-02": digitalocean_droplet.web02.ipv4_address
  }
}
